var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser')
var fs=require("fs");
var cors = require('cors');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

app.use(cors({
    origin: 'http://localhost:3000',
    credentials: true
}));
app.use(logger('dev'));
app.use(express.json());
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser('kfdfdlju45$'));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
var routes = fs.readdirSync('./routes');
for (var i = 0; i < routes.length; i++) {
    var file = routes[i].substr(0, routes[i].lastIndexOf('.'));
    app.use('/' + file, require('./routes/' + file));
}

module.exports = app;
