var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.status(200).send({
      "_embedded": {
          "languages": [
              {
                  "id": "97a40806-337b-4ed8-827d-d8d7dafc6d77",
                  "code": "br",
                  "name": "portuguese br",
                  "_links": {
                      "self": {
                          "href": "http://localhost:8080/languages/97a40806-337b-4ed8-827d-d8d7dafc6d77"
                      },
                      "language": {
                          "href": "http://localhost:8080/languages/97a40806-337b-4ed8-827d-d8d7dafc6d77"
                      }
                  }
              },
              {
                  "id": "d50f22d3-4206-4e0c-9ade-c9b3399991e5",
                  "code": "cz",
                  "name": "czech",
                  "_links": {
                      "self": {
                          "href": "http://localhost:8080/languages/d50f22d3-4206-4e0c-9ade-c9b3399991e5"
                      },
                      "language": {
                          "href": "http://localhost:8080/languages/d50f22d3-4206-4e0c-9ade-c9b3399991e5"
                      }
                  }
              }
          ]
      },
      "_links": {
          "self": {
              "href": "http://localhost:8080/languages"
          },
          "profile": {
              "href": "http://localhost:8080/profile/languages"
          }
      }
  });
});

router.get('/:id', function(req, res, next) {
  if(typeof (req.params.id) === 'undefined' || req.params.id == null){
    res.sendStatus(400);
  }else{
      let languages = [
          {
              "id": "97a40806-337b-4ed8-827d-d8d7dafc6d77",
              "code": "br",
              "name": "portuguese br",
              "_links": {
                  "self": {
                      "href": "http://localhost:8080/languages/97a40806-337b-4ed8-827d-d8d7dafc6d77"
                  },
                  "language": {
                      "href": "http://localhost:8080/languages/97a40806-337b-4ed8-827d-d8d7dafc6d77"
                  }
              }
          },
          {
              "id": "d50f22d3-4206-4e0c-9ade-c9b3399991e5",
              "code": "cz",
              "name": "czech",
              "_links": {
                  "self": {
                      "href": "http://localhost:8080/languages/d50f22d3-4206-4e0c-9ade-c9b3399991e5"
                  },
                  "language": {
                      "href": "http://localhost:8080/languages/d50f22d3-4206-4e0c-9ade-c9b3399991e5"
                  }
              }
          }
      ];
      let language = languages.filter(lang => {
        return lang.id === req.params.id;
      });
      if(language.length == 0){
        res.sendStatus(404);
      }else{
        res.status(200).send(language[0]);
      }
  }
});

router.get('/:id/subjects', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null){
        res.sendStatus(400);
    }else{
        let subjects = [
            {
                "id": "97a40806-337b-4ed8-827d-d8d7dafc6d77",
                "translation": "I",
                "name": "Ja"
            },
            {
                "id": "d50f22d3-4206-4e0c-9ade-c9b3399991e5",
                "translation": "You inf.",
                "name": "Ty"
            }
        ];
        if(typeof (req.query.q) !== 'undefined' && req.query.q !== null){
            let foundSubjects = subjects.filter(subject => {
                return subject.translation.indexOf(req.query.q) > -1;
            });
            res.status(200).send({
                "_embedded": foundSubjects
            });
        }else{
            if(subjects.length == 0){
                res.sendStatus(404);
            }else{
                res.status(200).send({
                    "_embedded": subjects
                });
            }
        }
    }
});

router.post('/:id/subjects', function(req, res, next) {
    res.status(201).send({
        "_embedded": {
            "id": "97a40806-337b-4ed8-827d-d8d7dafc6d15",
            "translation": req.body.translation,
            "name": req.body.name
        }
    });
});

router.put('/:id/subjects/:sub_id', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null || typeof (req.params.sub_id) === 'undefined' || req.params.sub_id == null){
        res.sendStatus(400);
    }else {
        res.status(200).send({
            "_embedded": {
                "id": req.params.sub_id,
                "translation": req.body.translation,
                "name": req.body.name
            }
        });
    }
});

router.delete('/:id/subjects/:sub_id', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null || typeof (req.params.sub_id) === 'undefined' || req.params.sub_id == null){
        res.sendStatus(400);
    }else{
        res.sendStatus(204);
    }
});

router.get('/:id/actions', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null){
        res.sendStatus(400);
    }else{
        let actions = [
            {
                "id": "97a40806-337b-4ed8-827d-d8d7dafc6d77",
                "translation": "Have",
                "name": "Myt"
            },
            {
                "id": "d50f22d3-4206-4e0c-9ade-c9b3399991e5",
                "translation": "Make",
                "name": "Delat"
            }
        ];
        if(typeof (req.query.q) !== 'undefined' && req.query.q !== null){
            let foundActions = actions.filter(action => {
                return action.translation.indexOf(req.query.q) > -1;
            });
            res.status(200).send({
                "_embedded": foundActions
            });
        }else {
            if (actions.length == 0) {
                res.sendStatus(404);
            } else {
                res.status(200).send({
                    "_embedded": actions
                });
            }
        }
    }
});

router.post('/:id/actions', function(req, res, next) {
    res.status(201).send({
        "_embedded": {
            "id": "97a40806-337b-4ed8-827d-d8d7dafc6d60",
            "translation": req.body.translation,
            "name": req.body.name
        }
    });
});

router.put('/:id/actions/:sub_id', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null || typeof (req.params.sub_id) === 'undefined' || req.params.sub_id == null){
        res.sendStatus(400);
    }else {
        res.status(200).send({
            "_embedded": {
                "id": req.params.sub_id,
                "translation": req.body.translation,
                "name": req.body.name
            }
        });
    }
});

router.delete('/:id/actions/:sub_id', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null || typeof (req.params.sub_id) === 'undefined' || req.params.sub_id == null){
        res.sendStatus(400);
    }else{
        res.sendStatus(204);
    }
});

router.get('/:id/objektconnections', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null){
        res.sendStatus(400);
    }else{
        let objektconnections = [
            {
                "id": "97a40806-337b-4ed8-827d-d8d7dafc6f12",
                "translation": "To direction",
                "name": "Do"
            },
            {
                "id": "d50f22d3-4206-4e0c-9ade-c9b3399991a8",
                "translation": "Between",
                "name": "Zemi"
            }
        ];
        if(typeof (req.query.q) !== 'undefined' && req.query.q !== null){
            let found = objektconnections.filter(ob => {
                return ob.translation.indexOf(req.query.q) > -1;
            });
            res.status(200).send({
                "_embedded": found
            });
        }else{
            if(objektconnections.length == 0){
                res.sendStatus(404);
            }else{
                res.status(200).send({
                    "_embedded": objektconnections
                });
            }
        }
    }
});

router.post('/:id/objektconnections', function(req, res, next) {
    res.status(201).send({
        "_embedded": {
            "id": "97a40806-337b-4ed8-827d-d8d7dafc6e58",
            "translation": req.body.translation,
            "name": req.body.name
        }
    });
});

router.put('/:id/objektconnections/:sub_id', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null || typeof (req.params.sub_id) === 'undefined' || req.params.sub_id == null){
        res.sendStatus(400);
    }else {
        res.status(200).send({
            "_embedded": {
                "id": req.params.sub_id,
                "translation": req.body.translation,
                "name": req.body.name
            }
        });
    }
});

router.delete('/:id/objektconnections/:sub_id', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null || typeof (req.params.sub_id) === 'undefined' || req.params.sub_id == null){
        res.sendStatus(400);
    }else{
        res.sendStatus(204);
    }
});

router.get('/:id/objekts', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null){
        res.sendStatus(400);
    }else{
        let objekts = [
            {
                "id": "97a40806-337b-4ed8-827d-d8d7dafc6f42",
                "translation": "Store",
                "name": "Obchod"
            },
            {
                "id": "d50f22d3-4206-4e0c-9ade-c9b3399994a8",
                "translation": "Street",
                "name": "Ulice"
            }
        ];
        if(typeof (req.query.q) !== 'undefined' && req.query.q !== null){
            let foundObjekts = objekts.filter(ob => {
                return ob.translation.indexOf(req.query.q) > -1;
            });
            res.status(200).send({
                "_embedded": foundObjekts
            });
        }else{
            if(objekts.length == 0){
                res.sendStatus(404);
            }else{
                res.status(200).send({
                    "_embedded": objekts
                });
            }
        }
    }
});

router.post('/:id/objekts', function(req, res, next) {
    res.status(201).send({
        "_embedded": {
            "id": "97a40806-337b-4ed8-827d-d8d7dafc6e98",
            "translation": req.body.translation,
            "name": req.body.name
        }
    });
});

router.put('/:id/objekts/:sub_id', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null || typeof (req.params.sub_id) === 'undefined' || req.params.sub_id == null){
        res.sendStatus(400);
    }else {
        res.status(200).send({
            "_embedded": {
                "id": req.params.sub_id,
                "translation": req.body.translation,
                "name": req.body.name
            }
        });
    }
});

router.delete('/:id/objekts/:sub_id', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null || typeof (req.params.sub_id) === 'undefined' || req.params.sub_id == null){
        res.sendStatus(400);
    }else{
        res.sendStatus(204);
    }
});

router.get('/:id/subjectactions', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null){
        res.sendStatus(400);
    }else{
        let subject_actions = [
            {
                "id": "97a40806-337b-4ed8-827d-d8d7dafc6e43",
                "subject": {
                    "id": "d50f21e4-4206-4e0c-9ade-c9b3399993a0",
                    "translation": "I",
                    "name": "Ja"
                },
                "action": {
                    "id": "d50f24a9-4206-4e0c-9adf-c9b3399993a0",
                    "translation": "Go by transportation",
                    "name": "Jet"
                }
            },
            {
                "id": "d50f22d3-4206-4e0c-9ade-c9b3399993a0",
                "subject": {
                    "id": "d50f21e4-4206-4e0c-9ca4-c9b3399993a0",
                    "translation": "You inf.",
                    "name": "Ty"
                },
                "action": {
                    "id": "d50f24a9-4206-4e0c-0adf-c9b3399993a1",
                    "translation": "Do / make",
                    "name": "Delat"
                }
            }
        ];
        if(typeof (req.query.q) !== 'undefined' && req.query.q !== null){
            let found = subject_actions.filter(ob => {
                return ob.subject.translation.indexOf(req.query.q) > -1 || ob.action.translation.indexOf(req.query.q) > -1;
            });
            res.status(200).send({
                "_embedded": found
            });
        }else {
            if (subject_actions.length == 0) {
                res.sendStatus(404);
            } else {
                res.status(200).send({
                    "_embedded": subject_actions
                });
            }
        }
    }
});

router.post('/:id/subjectactions', function(req, res, next) {
    res.status(201).send({
        "_embedded": {
            "id": "97a40806-337b-4ed8-827d-d8d7daac6e98",
            "subject": {
                "id": "d50f21e4-4206-4e0c-9ca4-c9b3399993a0",
                "translation": "You inf.",
                "name": "Ty"
            },
            "action": {
                "id": "d50f24a9-4206-4e0c-0adf-c9b3399993a1",
                "translation": "Do / make",
                "name": "Delat"
            }
        }
    });
});

router.put('/:id/subjectactions/:sub_id', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null || typeof (req.params.sub_id) === 'undefined' || req.params.sub_id == null){
        res.sendStatus(400);
    }else {
        res.status(200).send({
            "_embedded": {
                "id": req.params.sub_id,
                "subject": {
                    "id": "d50f21e4-4206-4e0c-9ca4-c9b3399993a0",
                    "translation": "You inf.",
                    "name": "Ty"
                },
                "action": {
                    "id": "d50f24a9-4206-4e0c-0adf-c9b3399993a1",
                    "translation": "Do / make",
                    "name": "Delat"
                }
            }
        });
    }
});

router.delete('/:id/subjectactions/:sub_id', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null || typeof (req.params.sub_id) === 'undefined' || req.params.sub_id == null){
        res.sendStatus(400);
    }else{
        res.sendStatus(204);
    }
});

router.get('/:id/actionobjekts', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null){
        res.sendStatus(400);
    }else{
        let actionobjekts = [
            {
                "id": "97a40806-337b-4ed8-826c-d8d7dafc6e43",
                "action": {
                    "id": "d50f24a9-4208-4e1d-9adf-c9b3399993a0",
                    "translation": "Drink",
                    "name": "Pit"
                },
                "objekt": {
                    "id": "d50f24a9-4208-1e1e-9adf-c9b3399993a0",
                    "translation": "Water",
                    "name": "Voda"
                }
            },
            {
                "id": "97a40806-337b-4ed8-826c-d8d7dafc7f44",
                "action": {
                    "id": "d50f24a9-4208-4e1d-9adf-c9b3399993b0",
                    "translation": "Go by transport",
                    "name": "Jet"
                },
                "objektconnection": {
                    "id": "d50f24a9-4208-1e1e-9adf-c9b3399993c0",
                    "translation": "To",
                    "name": "Do"
                },
                "objekt": {
                    "id": "d50f24a9-4208-1e1e-9adf-c9b3399993d0",
                    "translation": "Store",
                    "name": "Obchod"
                }
            }
        ];
        if(typeof (req.query.q) !== 'undefined' && req.query.q !== null){
            let found = actionobjekts.filter(ob => {
                return (ob.objekt.translation.indexOf(req.query.q) > -1
                        || ob.action.translation.indexOf(req.query.q) > -1
                        || ( typeof(ob.objektconnection) !== 'undefined'
                                && ob.objektconnection != null
                                && typeof(ob.objektconnection.translation) !== 'undefined'
                                && ob.objektconnection.translation != null
                                && ob.objektconnection.translation.indexOf(req.query.q) > -1));
            });
            res.status(200).send({
                "_embedded": found
            });
        }else {
            if(actionobjekts.length == 0){
                res.sendStatus(404);
            }else{
                res.status(200).send({
                    "_embedded": actionobjekts
                });
            }
        }
    }
});

router.post('/:id/actionobjekts', function(req, res, next) {
    res.status(201).send({
        "_embedded": {
            "id": "97a40806-337b-4ed8-826c-d8d7dafc7f87",
            "action": {
                "id": "d50f24a9-4208-4e1d-9adf-c9b3399993b2",
                "translation": "Go by transport",
                "name": "Jet"
            },
            "objektconnection": {
                "id": "d50f24a9-4208-1e1e-9adf-c9b3399993c2",
                "translation": "To",
                "name": "Do"
            },
            "objekt": {
                "id": "d50f24a9-4208-1e1e-9adf-c9b3399993d2",
                "translation": "Store",
                "name": "Obchod"
            }
        }
    });
});

router.put('/:id/actionobjekts/:sub_id', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null || typeof (req.params.sub_id) === 'undefined' || req.params.sub_id == null){
        res.sendStatus(400);
    }else {
        res.status(200).send({
            "_embedded": {
                "id": req.params.sub_id,
                "action": {
                    "id": "d50f24a9-4208-4e1d-9adf-c9b3399993b2",
                    "translation": "Go by transport",
                    "name": "Jet"
                },
                "objektconnection": {
                    "id": "d50f24a9-4208-1e1e-9adf-c9b3399993c2",
                    "translation": "To",
                    "name": "Do"
                },
                "objekt": {
                    "id": "d50f24a9-4208-1e1e-9adf-c9b3399993d2",
                    "translation": "Store",
                    "name": "Obchod"
                }
            }
        });
    }
});

router.delete('/:id/actionobjekts/:sub_id', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null || typeof (req.params.sub_id) === 'undefined' || req.params.sub_id == null){
        res.sendStatus(400);
    }else{
        res.sendStatus(204);
    }
});

router.post('/', function(req, res, next) {
    res.status(201).send({
        "_embedded": {
            "id": "97a40806-337b-4ed8-827d-d8d7dafc6d80",
            "code": req.body.code,
            "name": req.body.name,
            "_links": {
                "self": {
                    "href": "http://localhost:8080/languages/97a40806-337b-4ed8-827d-d8d7dafc6d80"
                },
                "language": {
                    "href": "http://localhost:8080/languages/97a40806-337b-4ed8-827d-d8d7dafc6d80"
                }
            }
        }
    });
});

router.put('/', function(req, res, next) {
    if(typeof (req.body.id) === 'undefined' || req.body.id == null){
        res.sendStatus(400);
    }else {
        res.status(200).send({
            "_embedded": {
                "id": req.body.id,
                "code": req.body.code,
                "name": req.body.name,
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/languages/97a40806-337b-4ed8-827d-d8d7dafc6d77"
                    },
                    "language": {
                        "href": "http://localhost:8080/languages/97a40806-337b-4ed8-827d-d8d7dafc6d77"
                    }
                }
            }
        });
    }
});

router.delete('/:id', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined' || req.params.id == null){
        res.sendStatus(400);
    }else{
        res.sendStatus(204);
    }
});

router.get('/:id/generate', function(req, res, next) {
    if(typeof (req.params.id) === 'undefined'
        || req.params.id == null
        || ((typeof (req.query.subjectactions) === 'undefined'
                || req.query.subjectactions == null)
            && (typeof (req.query.objektactions) === 'undefined'
                || req.query.objektactions == null))){
        res.sendStatus(400);
    }else{
        let subjectactions = [];
        let actionobjekts = [];
        let quantity = (typeof (req.query.quantity) === 'undefined' || req.query.quantity == null || req.query.quantity < 1 || req.query.quantity > 50)? 1 : req.query.quantity;
        if(typeof (req.query.subjectactions) !== 'undefined'
            && req.query.subjectactions != null){
            subjectactions = req.query.subjectactions.split(',');
        }
        if(typeof (req.query.actionobjekts) !== 'undefined'
            && req.query.actionobjekts != null){
            actionobjekts = req.query.actionobjekts.split(',');
        }
        let subject_actions = [
            {
                "id": "97a40806-337b-4ed8-827d-d8d7dafc6e43",
                "subject": {
                    "id": "d50f21e4-4206-4e0c-9ade-c9b3399993a0",
                    "translation": "I",
                    "name": "Ja"
                },
                "action": {
                    "id": "d50f24a9-4208-4e1d-9adf-c9b3399993a0",
                    "translation": "Drink",
                    "name": "Pit"
                }
            },
            {
                "id": "d50f22d3-4206-4e0c-9ade-c9b3399993a0",
                "subject": {
                    "id": "d50f21e4-4206-4e0c-9ca4-c9b3399993a0",
                    "translation": "You inf.",
                    "name": "Ty"
                },
                "action": {
                    "id": "d50f24a9-4208-4e1d-9adf-c9b3399993b0",
                    "translation": "Go by transport",
                    "name": "Jet"
                }
            }
        ];
        let action_objekts = [
            {
                "id": "97a40806-337b-4ed8-826c-d8d7dafc6e48",
                "action": {
                    "id": "d50f24a9-4208-4e1d-9adf-c9b3399993a0",
                    "translation": "Drink",
                    "name": "Pit"
                },
                "objekt": {
                    "id": "d50f24a9-4208-1e1e-9adf-c9b3399993a0",
                    "translation": "Water",
                    "name": "Voda"
                }
            },
            {
                "id": "97a40806-337b-4ed8-826c-d8d7dafc7f44",
                "action": {
                    "id": "d50f24a9-4208-4e1d-9adf-c9b3399993b0",
                    "translation": "Go by transport",
                    "name": "Jet"
                },
                "objektconnection": {
                    "id": "d50f24a9-4208-1e1e-9adf-c9b3399993c0",
                    "translation": "To",
                    "name": "Do"
                },
                "objekt": {
                    "id": "d50f24a9-4208-1e1e-9adf-c9b3399993d0",
                    "translation": "Store",
                    "name": "Obchod"
                }
            }
        ];
        let saFound = subject_actions.filter(sa => {
            return subjectactions.includes(sa.id);
        });
        let aoFound = action_objekts.filter(ao => {
            return actionobjekts.includes(ao.id);
        });
        let matches = [];
        if(true/*saFound.length > 0 && aoFound.length > 0*/){
            matches = [
                {
                    "action": {
                        "id": "d50f24a9-4208-4e1d-9adf-c9b3399993a0",
                        "translation": "Drink",
                        "name": "Pit"
                    },
                    "objekt": {
                        "id": "d50f24a9-4208-1e1e-9adf-c9b3399993a0",
                        "translation": "Water",
                        "name": "Voda"
                    },
                    "actionobjekt" : {
                        "id": "97a40806-337b-4ed8-826c-d8d7dafc6e48",
                        "action": {
                            "id": "d50f24a9-4208-4e1d-9adf-c9b3399993a0",
                            "translation": "Drink",
                            "name": "Pit"
                        },
                        "objekt": {
                            "id": "d50f24a9-4208-1e1e-9adf-c9b3399993a0",
                            "translation": "Water",
                            "name": "Voda"
                        }
                    },
                    "subjectaction" : {
                        "id": "97a40806-337b-4ed8-827d-d8d7dafc6e43",
                        "subject": {
                            "id": "d50f21e4-4206-4e0c-9ade-c9b3399993a0",
                            "translation": "I",
                            "name": "Ja"
                        },
                        "action": {
                            "id": "d50f24a9-4208-4e1d-9adf-c9b3399993a0",
                            "translation": "Drink",
                            "name": "Pit"
                        }
                    },
                    "subject": {
                        "id": "d50f21e4-4206-4e0c-9ade-c9b3399993a0",
                        "translation": "I",
                        "name": "Ja"
                    }
                },
                {
                    "action": {
                        "id": "d50f24a9-4208-4e1d-9adf-c9b3399993a0",
                        "translation": "Drink",
                        "name": "Pit"
                    },
                    "objekt": {
                        "id": "d50f24a9-4208-1e1e-9adf-c9b3399993a0",
                        "translation": "Water",
                        "name": "Voda"
                    },
                    "actionobjekt" : {
                        "id": "97a40806-337b-4ed8-826c-d8d7dafc6e48",
                        "action": {
                            "id": "d50f24a9-4208-4e1d-9adf-c9b3399993a0",
                            "translation": "Drink",
                            "name": "Pit"
                        },
                        "objekt": {
                            "id": "d50f24a9-4208-1e1e-9adf-c9b3399993a0",
                            "translation": "Water",
                            "name": "Voda"
                        }
                    },
                    "subjectaction" : {
                        "id": "97a40806-337b-4ed8-927d-d8d7dafc6e43",
                        "subject": {
                            "id": "d50f21e4-4206-4e0c-9ade-c9b3399993d4",
                            "translation": "You inf.",
                            "name": "Ty"
                        },
                        "action": {
                            "id": "d50f24a9-4208-4e1d-9adf-c9b3399993a0",
                            "translation": "Drink",
                            "name": "Pit"
                        }
                    },
                    "subject": {
                        "id": "d50f21e4-4206-4e0c-9ade-c9b3399993d4",
                        "translation": "You inf.",
                        "name": "Ty"
                    }
                },
                {
                    "action": {
                        "id": "d50f24a9-4208-4e1d-9adf-c9b3399993a0",
                        "translation": "Drink",
                        "name": "Pit"
                    },
                    "objekt": {
                        "id": "d50f24a9-4208-1e1e-9adf-c9b3399993a0",
                        "translation": "Water",
                        "name": "Voda"
                    },
                    "actionobjekt" : {
                        "id": "97a40806-337b-4ed8-826c-d8d7dafc6e48",
                        "action": {
                            "id": "d50f24a9-4208-4e1d-9adf-c9b3399993a0",
                            "translation": "Drink",
                            "name": "Pit"
                        },
                        "objekt": {
                            "id": "d50f24a9-4208-1e1e-9adf-c9b3399993a0",
                            "translation": "Water",
                            "name": "Voda"
                        }
                    },
                    "subjectaction" : {
                        "id": "97a40806-338b-4ed8-827d-d8d7dafc6e43",
                        "subject": {
                            "id": "d50f21e4-4206-4e0c-9ade-c9b3399998a0",
                            "translation": "Woman",
                            "name": "Zena"
                        },
                        "action": {
                            "id": "d50f24a9-4208-4e1d-9adf-c9b3399993a0",
                            "translation": "Drink",
                            "name": "Pit"
                        }
                    },
                    "subject": {
                        "id": "d50f21e4-4206-4e0c-9ade-c9b3399998a0",
                        "translation": "Woman",
                        "name": "Zena"
                    }
                }
            ];
        }else{
            res.status(200).send({
                "_embedded": []
            });
        }
        let therms = [];
        if(matches.length > 0){
            for(quantity; quantity > 0 && matches.length > 0; quantity--){
                let randIndex = Math.floor((Math.random() * matches.length));
                therms.push(matches[randIndex]);
                matches.splice(randIndex, 1);
            }
            res.status(200).send({
                "_embedded": therms
            });
        }else{
            res.status(200).send({
                "_embedded": []
            });
        }
    }
});

module.exports = router;
