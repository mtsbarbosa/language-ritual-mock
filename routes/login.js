const express = require('express');
const router = express.Router();
const cookieParser = require('cookie-parser');

/* GET users listing. */
router.get('/', function(req, res, next) {
    // read cookies

    let options = {
        maxAge: 1000 * 60 * 15, // would expire after 15 minutes
        httpOnly: true, // The cookie only accessible by the web server
        signed: true // Indicates if the cookie should be signed
    }

    // Set cookie
    res.cookie('JSESSIONID', '6834E611245AF2ED24EF4CC17D6ACAD4', options) // options is optional
    res.status(200).send('');
});

module.exports = router;
